rec <- read.table("recorridos1.csv", header = TRUE, sep = ",")
usu <- read.table("usuarios1.csv", header = TRUE, sep = ",")
edad <- table(usu$edad_usuario)
plot(edad, ylab = "cant", xlab = "edad")

summary(usu$edad_usuario)
boxplot(usu$edad_usuario, horizontal = T, xlab = "Edad")
summary(rec$distancia)
boxplot(rec$distancia)


origen <- table(rec$direccion_estacion_origen)
top_origen <- sort(origen, decreasing = T)[0:10] #10 estaciones mas usadas
barplot(top_origen,las=2, cex.names = 0.5, col = "red",
        main = "Las 10 estaciones mas usadas")



destino <- table(rec$direccion_estacion_destino)
top_destino <- sort(destino, decreasing = T)[0:10]
barplot(top_destino, las=2, cex.names = 0.5, col = "red",
        main = "Las 10 estaciones mas usadas") #10 estaciones mas usadas
table(rec$id_usuario)

ori_dest <- top_origen[names(top_destino)]
dest_ori <- top_destino[names(top_origen)]

barplot(table(top_origen, dest_ori))
barplot(rbind(top_origen, dest_ori), col = c("#1b98e0", "#353436"), beside=T)
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

estaciones <- union(rec$direccion_estacion_destino, rec$direccion_estacion_origen)
diferencia_uso <- function(estacion) {
  if (!is.na(origen[estacion])) {
    if (!is.na(destino[estacion]))
      return (destino[estacion] - origen[estacion])
    else
      return (-origen[estacion])
  }
  else {
    if (!is.na(destino[estacion]))
      return (destino[estacion])
    else
      return (0)
  }
}


diferencia_uso_estaciones <- sapply(estaciones, diferencia_uso, USE.NAMES=F) # diferencia de bicicletas en la estacion
top_estaciones <- sort(diferencia_uso_estaciones, decreasing=T)[1:10] # mas bicicletas en la estacion
bottom_estaciones <- sort(diferencia_uso_estaciones)[1:10] # menos bicicletas en la estacion
top_estaciones <- top_estaciones[order(origen[names(top_estaciones)], decreasing = T)]
bottom_estaciones <- bottom_estaciones[order(origen[names(bottom_estaciones)], decreasing = T)]

# origen y destino (mas bicicletas)
par(mar=c(12,4,4,1))
barplot(rbind(origen[names(top_estaciones)], destino[names(top_estaciones)]),
        beside=T, las=2, cex.names = 0.7, col = c("#1b98e0", "#353436"),
        main="Mas bicicletas en la estacion") # diferencias mas grandes
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

# origen y destino (menos bicicletas)
par(mar=c(12,4,4,1))
barplot(rbind(origen[names(bottom_estaciones)], destino[names(bottom_estaciones)]),
        beside=T, las=2, cex.names = 0.7, col = c("#1b98e0", "#353436"),
        main="Menos bicicletas en la estacion") # diferencias mas grandes
legend("topright",                                    # Add legend to barplot
       legend = c("Origen", "Destino"),
       fill = c("#1b98e0", "#353436"))

plot(sort(origen))

usos <- cbind(table(rec$direccion_estacion_destino), table(rec$direccion_estacion_origen))
usos <- cbind(rec$direccion_estacion_destino, rec$direccion_estacion_origen)

summary(rec$duracion_recorrido)
table(rec$dia)
hist(rec$distancia)
hist(rec$duracion_recorrido)

excedido <- c(rec$id_usuario, rec$duracion_recorrido)[rec$duracion_recorrido > 1800]
excedido
