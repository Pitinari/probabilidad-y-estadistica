---
title: "Informe TP"
output:
  pdf_document: default
  html_document: false
---

## Carga de datos

Cargamos los datos de las tablas de recorridos y usuarios.
```{r}
rec <- read.table("recorridos1.csv", header = TRUE, sep = ",", encoding="UTF-8")
usu <- read.table("usuarios1.csv", header = TRUE, sep = ",")
```

## Analisis de las distancias recorridas

Medidas resumen de las distancias
```{r}
distancia <- rec$distancia
rango_distancia <- trunc(range(distancia))

summary(distancia)
```

La distancia es una variable continua.\
Asumimos que la unidad de medida de distancia son metros y elegimos intervalos de clase de 100 metros.

Grafico de distribucion de distancias junto con boxplot.

```{r}
intervalos <- seq(from=rango_distancia[1], to=rango_distancia[2]+100, by=100)
hist(distancia, intervalos, main="", xlab="Distancia",
     ylab="Viajes", col="#87C353", border="#87C353",
     xaxp=c(intervalos[1], tail(intervalos, n=1), 5))
boxplot(distancia, horizontal=T, col="Red", add=T, 
        pars=list(boxwex=2, staplewex=1, outwex=1.5),
        axes=F)
abline(v=10000, col="#B03DE6")
text(10050, 10, "10km", col="#B03DE6", pos=4)
```

Vemos que mas del 75% porciento de los viajes recorren menos de 10 kilometros.

\pagebreak

## Analisis de las estaciones de origen y destino

Origen y destino son variables categoricas. \

```{r}
origen <- table(rec$direccion_estacion_origen)
destino <- table(rec$direccion_estacion_destino)
```

### Distribucion de uso de las estaciones de origen
```{r}
rango_origen <- range(origen)
plot(origen, axes=F, xlab="Estaciones", ylab="Uso", main="Uso de estaciones (origen)",
     col="#87C353", frame.plot=T)
axis(2, at=seq(from=0, to=rango_origen[2], by=5))
```
Vemos que el uso de estaciones es muy impredecible. \
Podemos ver la media de viajes que eligen como origen cada estacion. \

```{r}
viajes_origen <- as.numeric(origen)
round(mean(viajes_origen), 2)
```

En promedio, 4 casi 5 bicis salen de cada estacion. \

Vemos la estacion que mas veces se eligio como origen. \

```{r}
names(which.max(origen))
```

### Distribucion de uso de las estaciones de destino
```{r}
rango_destino <- range(destino)
plot(destino, axes=F, xlab="Estaciones", ylab="Uso", main="Uso de estaciones (destino)",
     col="#87C353", frame.plot=T)
axis(2, at=seq(from=0, to=rango_destino[2], by=5))
```


Podemos ver la media de viajes que eligen como destino cada estacion. \
```{r}
viajes_destino <- as.numeric(destino)
round(mean(viajes_destino), 2)
```

Al igual que con las bicis que salen, en promedio, 4 casi 5 bicis entran en cada
estacion. \

Vemos la estacion que mas veces fue destino. \
```{r}
names(which.max(destino))
```

\pagebreak

## Analisis de duracion de recorrido

La duracion del recorrido es una variable continua. \
Asumimos que la unidad de medida son los segundos y adoptamos intervalos de clase de 100 segundos.

```{r}
duracion_rec <- rec$duracion_recorrido
rango_duracion <- trunc(range(duracion_rec))
intervalos <- seq(from=0, to=rango_duracion[2]+100, by=100)
tick_marks <- seq(from=0, to=rango_duracion[2]+1800, by=1800)

hist_duracion <- hist(duracion_rec, intervalos, xlab="Duracion (s)", ylab="Viajes", 
                      main=NULL, col="#87C353", border="#87C353", xaxt="n",
                      xlim=c(0,tail(tick_marks, n=1)))

axis(side=1, at=tick_marks, labels=tick_marks)
```

Vemos que la mayor parte de los viajes transcurre en menos de una hora. Este es el tiempo maximo antes de que se cobren extras por el uso de las bicicletas. \

\pagebreak

## Analisis de dia

El dia del recorrido es una variable categorica. \
Analizamos su comportamiento con un grafico de bastones.

```{r}
dia <- table(rec$dia)
plot(dia, xlab="Dia", ylab="Viajes", col="#9B433B", frame.plot=T)
```

Vemos que la media de viajes por dia ronda los 120.
```{r}
viajes_dia <- as.numeric(dia)
round(mean(viajes_dia), 2)
```

\pagebreak

## Analisis de genero

Genero es una variable categorica. \
Podemos analizar la distribucion de genero a traves de los datos de usuarios y a traves de los datos de recorridos. \
Empezamos con los datos de usuarios. \

```{r}
genero <- table(usu$genero_usuario)
barplot(genero, col="#4F9E43", ylab="Usuarios", axes=F)
axis(2, at=trunc(seq(from=0, to=max(genero), length.out=6)))
```

Vemos que hay una ligera mayoria de mujeres y un gran numero de usuarios de genero "otro". \

Analizamos los datos de recorrido ahora. \
```{r}
usu_rec <- merge(usu, rec)
genero_viaje <- table(usu_rec$genero_usuario) # tabla de genero por los viajes
barplot(genero_viaje, col="#4F9E43", ylab="Viajes", axes=F)
axis(2, at=trunc(seq(from=0, to=max(genero_viaje), length.out=6)))
```

Vemos que las cantidades de viajes son mas parecidas entre los hombres y mujeres. \
Podemos obtener la cantidad de viajes media por persona dependiendo de su genero
dividiendo las cantidades de los dos previos graficos respectivamente. \
```{r}
media_viajes_genero <- as.numeric(genero_viaje) / as.numeric(genero)
barplot(media_viajes_genero, col="#4F9E43", ylab="Viajes", space=0.2)
axis(1, at=c(0.7,1.9,3.1), labels=names(genero), tick=F)
```

Vemos que los hombres usan en promedio mas veces las bicicletas que las mujeres
y los usuarios con "otro" genero.

\pagebreak

## Analisis de edad

La edad es una variable continua. Adoptamos intervalos de clase de 10 años. \
Esta variable tambien se puede analizar segun los datos de viajes y los datos
de usuarios, pero solo vamos a hacerlo para los datos de usuario. \

```{r}
edad <- usu$edad_usuario
rango_edad <- trunc(range(edad))
intervalos <- seq(from=rango_edad[1], to=rango_edad[2]+10, by=10)
hist(edad, intervalos, xlab="Edad", ylab="Usuarios", col="#87C353", border="#87C353",
     axes=F, main="")
axis(1, at=intervalos)
axis(2)
```

Podemos ver que las franjas de edad de 18-28 y de 28-38 son las franjas con mayor numero de usuarios. \

Medidas resumen de las edades

```{r}
edad <- table(usu$edad_usuario)
summary(usu$edad_usuario)
```

\pagebreak

## Analisis de distancias segun el genero

Se hace un analisis bivariado de las distancias recorridas segun el genero. Se hace
uso de intervalos de clase de 1000 segundos. \

```{r}
genero <- usu_rec$genero_usuario
distancia <- usu_rec$distancia
intervalos <- intervalos <- seq(from=min(distancia), to=max(distancia)+1000, by=1000)
genero_dist <- table(genero, cut(distancia, breaks=intervalos, include.lowest=T))
barplot(genero_dist, beside=T, col=c("red", "green", "blue"), xaxt="n", 
        xlab="Distancia", ylab="Usuarios")
legend("topright",                                    # Add legend to barplot
       legend = c("Mujeres", "Hombres", "Otros"),
       fill = c("red", "green", "blue"))

marcas <- cbind(
  seq(from=0, to=max(distancia)+1000, length.out=5),
  seq(from=1, to=dim(genero_dist)[2]*4, length.out=5))
ag_marca <- function(marca) {
  marca <- trunc(marca)
  mtext(marca[1], side=1, line=0, at=marca[2])
}
apply(marcas, 1, ag_marca)
```